package com.tradions.android_mvvm.di.components;

import android.app.Application;

import com.tradions.android_mvvm.AndroidMVVMApp;
import com.tradions.android_mvvm.di.builder.ActivityBuilderModule;
import com.tradions.android_mvvm.di.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {
        AppModule.class,
        AndroidInjectionModule.class,
        ActivityBuilderModule.class})
public interface AppComponent {

    void inject(AndroidMVVMApp androidMVVMApp);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

}