package com.tradions.android_mvvm.di.builder;


import com.tradions.android_mvvm.ui.main.MainActivity;
import com.tradions.android_mvvm.ui.main.MainActivityModule;
import com.tradions.android_mvvm.ui.main.products.ProductsFragmentProvider;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModule {

    @SuppressWarnings("unused")
    @ContributesAndroidInjector( modules = {
            ProductsFragmentProvider.class,
            MainActivityModule.class
        })
    abstract MainActivity mainActivity();

}
