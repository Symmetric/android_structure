package com.tradions.android_mvvm.utils;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;


public class BooleanDeserializer implements JsonDeserializer<Boolean> {

    private static final String TAG = "boolean_deserializer";
    public Boolean deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        try {
            int code = json.getAsInt();
            Log.d(TAG, "deserialize: " + code);
            return code == 0 ? false :
                    code == 1 ? true :
                            null;
        } catch (Exception e) {
            if (e instanceof NumberFormatException) {
                return json.getAsBoolean();
            }
            else return null;
        }


    }

}
