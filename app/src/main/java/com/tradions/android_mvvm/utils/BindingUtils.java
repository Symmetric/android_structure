/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.tradions.android_mvvm.utils;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tradions.android_mvvm.data.model.db.ProductAllRelation;
import com.tradions.android_mvvm.data.model.db.Size;
import com.tradions.android_mvvm.data.remote.Resource;
import com.tradions.android_mvvm.ui.custom.GlideApp;
import com.tradions.android_mvvm.ui.main.products.ProductAdapter;
import com.tradions.android_mvvm.ui.main.products.SizeAdapter;

import java.util.List;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import static com.tradions.android_mvvm.data.remote.ApiConstants.BASE_URL;


public final class BindingUtils {

    private static final String TAG = "binding_utils";

    private BindingUtils() {
        // This class is not publicly instantiable
    }

    @BindingAdapter({"productsAdapter"})
    public static void addProductItems(RecyclerView recyclerView, Resource<List<ProductAllRelation>> items) {
        ProductAdapter adapter = (ProductAdapter) recyclerView.getAdapter();
        if (adapter != null && items != null ) {
            adapter.clearItems();
            adapter.addItems(items.data);
        }
    }

    @BindingAdapter({"sizesAdapter"})
    public static void addSizeItems(RecyclerView recyclerView, List<Size> items) {
        SizeAdapter adapter = (SizeAdapter) recyclerView.getAdapter();
        if (adapter != null && items != null) {
            adapter.addItems(items);
        }
    }

    @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Context context = imageView.getContext();
        String imageUrl = BASE_URL + url;
        GlideApp.with(context)
                .load(imageUrl)
                .into(imageView)
        ;
    }

    @BindingAdapter("activated")
    public static void setActivated(View view, boolean activated) {
        view.setActivated(activated);
    }

    @BindingAdapter("currency")
    public static void setCurrency(TextView textView, String cost) {
        if(cost != null) {
            textView.setText(cost.concat(" SP"));
        }
    }

    @BindingAdapter("currency")
    public static void setCurrency(TextView view, float value) {
        if (Float.isNaN(value)) view.setText("");
        else view.setText(String.valueOf(value).concat(" SP"));
    }

    @BindingAdapter("android:text")
    public static void setFloat(TextView view, float value) {
        if (Float.isNaN(value)) view.setText("");
        else view.setText(String.valueOf(value));
    }

    @InverseBindingAdapter(attribute = "android:text")
    public static float getFloat(TextView view) {
        String num = view.getText().toString();
        if(num.isEmpty()) return 0.0F;
        try {
            return Float.parseFloat(num);
        } catch (NumberFormatException e) {
            return 0.0F;
        }
    }
}
