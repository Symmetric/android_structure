package com.tradions.android_mvvm.ui.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class CircularView extends View {


    public CircularView(Context context, AttributeSet attributeSet){
        super(context, attributeSet);
        init();


    }
    Paint paint;
    public void init(){
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
        paint.setShadowLayer(30, 0, 0, Color.RED);

    }
    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawCircle(getWidth()/2, getHeight()/2,50, paint);
}
}
