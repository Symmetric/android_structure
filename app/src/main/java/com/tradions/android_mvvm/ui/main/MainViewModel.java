package com.tradions.android_mvvm.ui.main;

import android.util.Log;

import com.tradions.android_mvvm.ui.base.BaseViewModel;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

public class MainViewModel extends BaseViewModel<MainNavigator> {


    public MutableLiveData<Long> selectedGenderId;


    @Inject
    public MainViewModel() {

        Log.d("SAED", "MainViewModel: I am here");

        selectedGenderId = new MutableLiveData<>();

        fetchData();
    }

    public void fetchData(){
//        genders.setValue(mGenderRepository.loadAll().getValue());
    }

}
