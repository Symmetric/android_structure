package com.tradions.android_mvvm.ui.base;

public interface ToolbarActionListener {

    void onBackPressedAction();

    void onGiftAction();

    void onBasketAction();

    void onFilterAction();
}
