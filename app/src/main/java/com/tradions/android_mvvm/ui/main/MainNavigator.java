package com.tradions.android_mvvm.ui.main;

import com.tradions.android_mvvm.ui.base.BaseNavigator;

public interface MainNavigator extends BaseNavigator {

    void home();

    void offers();

    void brands();

    void favorites();

    void profile();

    void products(long brandId);

    void bag();

}
