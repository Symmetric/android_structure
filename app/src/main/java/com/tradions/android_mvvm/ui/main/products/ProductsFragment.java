package com.tradions.android_mvvm.ui.main.products;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.tradions.android_mvvm.R;
import com.tradions.android_mvvm.data.model.db.Product;
import com.tradions.android_mvvm.data.model.db.ProductAllRelation;
import com.tradions.android_mvvm.data.remote.Resource;
import com.tradions.android_mvvm.databinding.ProductsFragmentBinding;
import com.tradions.android_mvvm.ui.base.BaseFragment;

import java.util.List;

import javax.inject.Inject;

import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


public class ProductsFragment extends BaseFragment<ProductsFragmentBinding, ProductsViewModel> implements
        ProductsNavigator, ProductAdapter.ProductAdapterListener {
    private static final String BRAND_ID_KEY = "brand_id_key";
    private static String TAG  = "products_fragment";

    @Inject
    ProductAdapter productAdapter;

    private long brandId ;
    private int pageNumber;

    public static ProductsFragment newInstance(long brandId) {
        ProductsFragment productsFragment = new ProductsFragment();
        Bundle args = new Bundle();
        args.putLong(BRAND_ID_KEY, brandId);

        productsFragment.setArguments(args);
        return productsFragment;

    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.products_fragment;
    }

    @Override
    public Class<ProductsViewModel> getViewModel() {
        return ProductsViewModel.class;
    }

    @Override
    public void setUpView() {

        pageNumber = 0;
        mViewModel.setNavigator(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mViewDataBinding.productsList.setLayoutManager(layoutManager);
        mViewDataBinding.productsList.setItemAnimator(new DefaultItemAnimator());
        mViewDataBinding.productsList.setAdapter(productAdapter);
        productAdapter.setListener(this);

        mViewDataBinding.pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(getActivity(), "I am refreshing", Toast.LENGTH_SHORT).show();
                ProductsFragment.this.onRefresh();
            }
        });
    }

    @Override
    public void emptyIntent() {
        if(getArguments() != null) {
            this.brandId = getArguments().getLong(BRAND_ID_KEY, -1);
        } else {
            Log.d("product", "onViewCreated: getargumens I am null" );
        }
    }

    @Override
    public void fetchData() {
        mViewModel.fetchData(this.brandId, this.pageNumber);
    }

    public void onRefresh(){
        mViewModel.refresh(this.brandId, this.pageNumber);
    }

    @Override
    public void listenToVariables() {
        mViewModel.items.observe(this, new Observer<Resource<List<ProductAllRelation>>>() {
            @Override
            public void onChanged(Resource<List<ProductAllRelation>> listResource) {
                if(listResource.data != null ){
                    if(listResource.data.size() <= productAdapter.getItemCount()) {
                        productAdapter.updateItems(listResource.data);
                    }
                    else{
                        productAdapter.addItems(listResource.data);
                    }
                }

                switch (listResource.status){
                    case ERROR:
                        break;
                    case SUCCESS:
                        mViewDataBinding.pullToRefresh.setRefreshing(false);
                        break;
                    case LOADING:
                        Toast.makeText(getActivity(), "I am loading data", Toast.LENGTH_SHORT).show();
                        break;
                }

            }
        });

    }


    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void onRetryClick() {

    }

    @Override
    public void onItemFavoriteClick(Product product) {
        mViewModel.favorite(product);
    }

    @Override
    public void onItemClick(long productId) {

    }
}
