package com.tradions.android_mvvm.ui.main.products;



import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.Module;
import dagger.Provides;

@Module
public class ProductsFragmentModule {


    @Provides
    ProductAdapter provideProductAdapter(){
        return new ProductAdapter(new ArrayList<>());
    }

    @Provides
    SizeAdapter provideSizeAdapter(){
        return new SizeAdapter(new ArrayList<>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(ProductsFragment fragment) {
        return new LinearLayoutManager(fragment.getActivity(), LinearLayoutManager.HORIZONTAL, false);
    }

}
