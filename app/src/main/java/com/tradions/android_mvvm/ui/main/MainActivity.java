package com.tradions.android_mvvm.ui.main;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.tradions.android_mvvm.BR;
import com.tradions.android_mvvm.R;
import com.tradions.android_mvvm.databinding.ActivityMainBinding;
import com.tradions.android_mvvm.ui.base.BaseActivity;
import com.tradions.android_mvvm.ui.main.products.ProductsFragment;
import com.tradions.android_mvvm.utils.FragmentUtils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> implements
        MainNavigator,
        BottomNavigationView.OnNavigationItemSelectedListener
        , View.OnClickListener {


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public Class<MainViewModel> getViewModel() {
        return MainViewModel.class;
    }

    @Override
    public void setUpView() {
        setSupportActionBar(this.mViewDataBinding.myToolbar);

        this.mViewDataBinding.bottomNavigation.setOnNavigationItemSelectedListener(this);

        this.mViewDataBinding.bottomNavigation.setItemIconTintList(null);

        mViewModel.setNavigator(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        mViewDataBinding.icBackBtn.setOnClickListener(this);
        mViewDataBinding.icBasket.setOnClickListener(this);
        mViewDataBinding.icFilter.setOnClickListener(this);
        mViewDataBinding.icGift.setOnClickListener(this);
    }

    @Override
    public void emptyIntent() {

    }

    @Override
    public void fetchData() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        this.products(1);
//        this.home();
        this.mViewDataBinding.bottomNavigation.setSelectedItemId(R.id.home_action);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void listenToVariables() {
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void home() {
    }
    @Override
    public void offers() {

    }

    @Override
    public void brands() {
    }

    @Override
    public void favorites() {

    }

    @Override
    public void profile() {

    }

    @Override
    public void products(long brandId) {
        FragmentUtils.replaceFragment(this,
                ProductsFragment.newInstance(brandId),
                this.mViewDataBinding.fragmentLayout.getId(),
                true,
                FragmentUtils.TRANSITION_FADE_IN_OUT);

    }

    @Override
    public void bag() {
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()){
            case R.id.home_action:
                this.products(2);
                break;
            case R.id.brands_action:
                this.products(2);
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ic_basket:
                this.bag();
                break;
        }
    }
}
