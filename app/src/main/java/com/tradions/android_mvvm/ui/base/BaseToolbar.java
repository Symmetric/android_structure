package com.tradions.android_mvvm.ui.base;

public interface BaseToolbar {

    void setFilterActionVisibility();

    void setBasketActionVisibility();

    void setGiftActionVisibility();

    void setLogoVisibility();

    void setMainTitle(String title);

    void setSideTitle(String title, boolean showBackBtn);

    void setActionListener(ToolbarActionListener actionListener);
}
