package com.tradions.android_mvvm.ui.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;

public class CircularTextView extends AppCompatTextView
{
    private float strokeWidth;
    int strokeColor,solidColor;

    private float shadowRadius;
    private int shadowColor = Color.BLACK;
    private CircularImageView.ShadowGravity shadowGravity = CircularImageView.ShadowGravity.BOTTOM;

    private Paint paintBorder;

    Paint paint;
    public CircularTextView(Context context) {
        super(context);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
    }

    public CircularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircularTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public void draw(Canvas canvas) {
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        Paint circlePaint = new Paint();
        circlePaint.setColor(solidColor);
        circlePaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        circlePaint.setShadowLayer(5, 0, 0, Color.GRAY);

        Paint strokePaint = new Paint();
        strokePaint.setColor(strokeColor);
        strokePaint.setFlags(Paint.ANTI_ALIAS_FLAG);
//        strokePaint.setShadowLayer(5, 0, 0, Color.GRAY);

        int  h = this.getHeight();
        int  w = this.getWidth();

        int diameter = ((h > w) ? h : w);
        int radius = (diameter/2) - 5;

        this.setHeight(diameter);
        this.setWidth(diameter);



        Paint textPaint = new Paint();
        canvas.drawCircle(diameter / 2 , diameter / 2, radius, strokePaint);

        canvas.drawCircle(diameter / 2, diameter / 2, radius - strokeWidth, circlePaint);

        super.draw(canvas);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    }

    public void setStrokeWidth(int dp)
    {
        float scale = getContext().getResources().getDisplayMetrics().density;
        strokeWidth = dp*scale;

    }

    public void setStrokeColor(String color)
    {
        strokeColor = Color.parseColor(color);
    }

    public void setSolidColor(String color)
    {
        solidColor = Color.parseColor(color);

    }

    private void drawShadow(float shadowRadius, int shadowColor) {
        this.shadowRadius = shadowRadius;
        this.shadowColor = shadowColor;
        setLayerType(LAYER_TYPE_SOFTWARE, paintBorder);

        float dx = 0.0f;
        float dy = 0.0f;

        switch (shadowGravity) {
            case CENTER:
                dx = 0.0f;
                dy = 0.0f;
                break;
            case TOP:
                dx = 0.0f;
                dy = -shadowRadius / 2;
                break;
            case BOTTOM:
                dx = 0.0f;
                dy = shadowRadius / 2;
                break;
            case START:
                dx = -shadowRadius / 2;
                dy = 0.0f;
                break;
            case END:
                dx = shadowRadius / 2;
                dy = 0.0f;
                break;
        }

        paintBorder.setShadowLayer(shadowRadius, dx, dy, shadowColor);
    }

}

