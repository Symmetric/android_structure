package com.tradions.android_mvvm.ui.main.products;

import com.tradions.android_mvvm.data.model.db.Size;

import androidx.databinding.ObservableField;
import androidx.lifecycle.ViewModel;

public class SizeItemViewModel extends ViewModel {
    public final Size mItem;

    public final ObservableField<String> code;

    public final int quantity;

    public SizeItemViewModel(Size item, ItemViewModelListener listener) {
        this.mItem = item;

        this.code = new ObservableField<>(mItem.getCode());

        this.quantity = mItem.getQuantity();
    }

    public interface ItemViewModelListener{
    }

}
