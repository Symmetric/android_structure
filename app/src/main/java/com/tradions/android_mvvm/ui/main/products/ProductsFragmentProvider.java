package com.tradions.android_mvvm.ui.main.products;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ProductsFragmentProvider {

    @ContributesAndroidInjector(modules = ProductsFragmentModule.class)
    abstract ProductsFragment provideProductsFragmentFactory();
}
