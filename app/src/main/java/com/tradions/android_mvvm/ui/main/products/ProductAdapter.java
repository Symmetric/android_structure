/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.tradions.android_mvvm.ui.main.products;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.tradions.android_mvvm.data.model.db.Product;
import com.tradions.android_mvvm.data.model.db.ProductAllRelation;
import com.tradions.android_mvvm.databinding.ProductListItemBinding;
import com.tradions.android_mvvm.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class ProductAdapter extends RecyclerView.Adapter<BaseViewHolder> {


    private static final String TAG = "SAED";
    private List<ProductAllRelation> items;

    private ProductAdapterListener mListener;

//    @Inject
//    SizeAdapter sizeAdapter;

//    @Inject
    LinearLayoutManager mLayoutManager;

    public ProductAdapter(List<ProductAllRelation> list) {
        this.items = list;
    }

    @Override
    public int getItemCount() {
            return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position, false);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ProductListItemBinding viewBinding = ProductListItemBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false);

        mLayoutManager = new LinearLayoutManager(parent.getContext(), LinearLayout.HORIZONTAL, false);
        viewBinding.sizesList.setLayoutManager(mLayoutManager);
        viewBinding.sizesList.setItemAnimator(new DefaultItemAnimator());
        viewBinding.sizesList.setAdapter(new SizeAdapter(new ArrayList<>()));

        return new viewHolder(viewBinding);
    }
    public void updateItems(List<ProductAllRelation> list) {
        if(items != null && list != null) {
            items.clear();
            items.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void addItems(List<ProductAllRelation> list) {
        if(items != null && list != null) {
            items.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void clearItems() {
        items.clear();
    }

    public void setListener(ProductAdapterListener listener) {
        this.mListener = listener;
    }

    public interface ProductAdapterListener {

        void onRetryClick();

        void onItemFavoriteClick(Product product);

        void onItemClick(long productId);
    }

    public class viewHolder extends BaseViewHolder implements ProductItemViewModel.ItemViewModelListener {

        private ProductListItemBinding mBinding;

        private ProductItemViewModel mViewModel;

        public viewHolder(ProductListItemBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position, Boolean isActivated) {
            final ProductAllRelation item = items.get(position);
            mViewModel = new ProductItemViewModel(item, this);
            mBinding.setViewModel(mViewModel);


            SizeAdapter sizeAdapter = new SizeAdapter(mViewModel.sizes);

            if((SizeAdapter)mBinding.sizesList.getAdapter() != null && mViewModel.sizes != null){
                ((SizeAdapter)mBinding.sizesList.getAdapter()).addItems(mViewModel.sizes);
            }

            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }

        @Override
        public void onItemClick(Product item) {
            mListener.onItemClick(item.getId());
//            Toast.makeText(itemView.getContext(), item.getName(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onItemFavorite(Product item) {
            mListener.onItemFavoriteClick(item);

        }

        @Override
        public void onItemBasket(Product item) {

        }
    }

}