package com.tradions.android_mvvm.ui.custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tradions.android_mvvm.R;
import com.tradions.android_mvvm.data.model.others.BaseSpinnerModel;
import com.tradions.android_mvvm.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

public class BaseTextSpinnerAdapter<T extends BaseSpinnerModel> extends ArrayAdapter<BaseViewHolder> {

    private final String TAG = "base_text_spinner";
    public List<T> mItems;
    public LayoutInflater inflater;
    public int resource;
    public String label;


    @Override
    public int getCount() {
        return mItems.size();
    }

    public BaseTextSpinnerAdapter(@NonNull Context context, int resource, String label) {
        super(context, resource);

        this.inflater = (LayoutInflater.from(context));
        this.mItems = new ArrayList<>();
        updateAll(new ArrayList<>());
        this.resource = resource;
        this.label = label;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        convertView = inflater.inflate(this.resource, null);
        TextView textView = ((TextView)convertView.findViewById(R.id.spinner_text));

        if(!isEnabled(position)) {
            textView.setTextColor(textView.getContext().getResources().getColor(R.color.gray));
        } else{
            textView.setTextColor(textView.getContext().getResources().getColor(R.color.colorAccent));
        }

        if(mItems.get(position) == null) {
            textView.setText(this.label);
        } else {
            textView.setText(mItems.get(position).getName());
        }


        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(resource, parent, false);
        TextView textView = ((TextView)convertView.findViewById(R.id.spinner_text));

        if(!isEnabled(position)) {
            textView.setTextColor(textView.getContext().getResources().getColor(R.color.light_gray));
        } else{
            textView.setTextColor(textView.getContext().getResources().getColor(R.color.colorAccent));
        }

        if(mItems.get(position) == null) {
            textView.setText(this.label);
        } else {
            textView.setText(mItems.get(position).getName());
        }


        return convertView;

    }

    /**
     * return object id if not null if
     * null will return -1
     * @param position
     * @return object id otherwise -1
     */
    @Override
    public long getItemId(int position) {
        if(mItems.get(position) != null) {
            return mItems.get(position).getObjectId();
        }
        return -1;
    }

    @Override
    public boolean isEnabled(int position) {
        return mItems.get(position) != null;
    }

    public void updateAll(List<T> items){
        if(items != null) {
            this.mItems.clear();
            this.mItems.addAll(items);
        }
        this.mItems.add(0,null);
        notifyDataSetChanged();
    }

    public T getSelectedItem(long id){
        int i = 0;
        int index = i;
        for (T item: this.mItems) {
            if(item != null && item.getObjectId() == id) {
                return item;
            }
            i++;
        }
        return null;
    }
}

