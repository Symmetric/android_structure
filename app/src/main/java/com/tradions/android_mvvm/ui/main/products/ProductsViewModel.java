package com.tradions.android_mvvm.ui.main.products;

import android.util.Log;

import com.tradions.android_mvvm.data.model.db.Product;
import com.tradions.android_mvvm.data.model.db.ProductAllRelation;
import com.tradions.android_mvvm.data.remote.Resource;
import com.tradions.android_mvvm.data.remote.repository.ProductRepository;
import com.tradions.android_mvvm.ui.base.BaseViewModel;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;


public class ProductsViewModel extends BaseViewModel<ProductsNavigator> {

    public LiveData<Resource<List<ProductAllRelation>>> items = new MutableLiveData<>();
    ProductRepository mProductRepository;


    @Inject
    public ProductsViewModel( ProductRepository productRepository) {

        this.mProductRepository = productRepository;
//        fetchData(1,0);
        this.items = new MutableLiveData<>();

    }

    public void fetchData(long brandId, int page){
        items = this.mProductRepository.loadAll(brandId,page);
    }

    public void refresh(long brandId, int page){
        this.mProductRepository.refreshLoadAll(brandId,page);
    }

    public void favorite(Product product){
        if(product.isFavorite()) {
            this.mProductRepository.unFavorite(product.getId());
        } else{
            this.mProductRepository.favorite(product.getId());
        }
    }

}
