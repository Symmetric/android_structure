/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.tradions.android_mvvm.ui.main.products;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.tradions.android_mvvm.R;
import com.tradions.android_mvvm.data.model.db.Size;
import com.tradions.android_mvvm.databinding.SizeItemBinding;
import com.tradions.android_mvvm.ui.base.BaseViewHolder;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;


public class SizeAdapter extends RecyclerView.Adapter<BaseViewHolder> {


    private static final String TAG = "SIZE";
    private List<Size> items;

    private SizeAdapterListener mListener;

    public SizeAdapter(List<Size> list) {
        Log.d(TAG, "getItemViewType: " + list.size());
        this.items = list;
    }

    @Override
    public int getItemCount() {
            return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position, false);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SizeItemBinding viewBinding = SizeItemBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false);
        Log.d("SIZE", "onCreateViewHolder: ");

        return new viewHolder(viewBinding);
    }

    public void addItems(List<Size> list) {
        if(items != null && list != null) {
            items.clear();
            items.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void clearItems() {
        items.clear();
    }

    public void setListener(SizeAdapterListener listener) {
        this.mListener = listener;
    }

    public interface SizeAdapterListener {

    }

    public class viewHolder extends BaseViewHolder implements SizeItemViewModel.ItemViewModelListener {

        private SizeItemBinding mBinding;

        private SizeItemViewModel mViewModel;

        public viewHolder(SizeItemBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position, Boolean isActivated) {
            final Size item = items.get(position);
            mViewModel = new SizeItemViewModel(item, this);
            mBinding.setViewModel(mViewModel);

            if(mViewModel.quantity > 0){
                mBinding.sizeCode.setBackground(
                        mBinding.getRoot().getContext().getResources().getDrawable(R.drawable.rectangle_accent_stroke_radius_50));
                mBinding.sizeCode.setTextColor(
                        mBinding.getRoot().getContext().getResources().getColor(R.color.colorAccent));
            }
            else{
                mBinding.sizeCode.setBackground(
                        mBinding.getRoot().getContext().getResources().getDrawable(R.drawable.rectangle_light_gray_stroke_radius_50));
                mBinding.sizeCode.setTextColor(
                        mBinding.getRoot().getContext().getResources().getColor(R.color.light_gray));
            }
            // Immediate Binding
            // When a variable or observable changes, the binding will be scheduled to change before
            // the next frame. There are times, however, when binding must be executed immediately.
            // To force execution, use the executePendingBindings() method.
            mBinding.executePendingBindings();
        }
    }

}