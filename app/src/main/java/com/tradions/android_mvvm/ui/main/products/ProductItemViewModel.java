package com.tradions.android_mvvm.ui.main.products;

import com.tradions.android_mvvm.data.model.db.Product;
import com.tradions.android_mvvm.data.model.db.ProductAllRelation;
import com.tradions.android_mvvm.data.model.db.Size;

import java.util.List;

import androidx.databinding.ObservableField;
import androidx.lifecycle.ViewModel;

public class ProductItemViewModel extends ViewModel {
    public final ProductAllRelation mItem;

    public final ItemViewModelListener modelListener;

    public final ObservableField<String> thumbnail;

    public final ObservableField<String> price;

    public final ObservableField<String> name;

    public final ObservableField<Boolean> isFavorite;

    public List<Size> sizes;


    public ProductItemViewModel(ProductAllRelation item, ItemViewModelListener listener) {
        this.mItem = item;
        this.modelListener = listener;

        this.thumbnail = new ObservableField<>(mItem.product.getThumbnail());
        this.price = new ObservableField<>(mItem.product.getPrice());
        this.name = new ObservableField<>(mItem.product.getName());
        this.isFavorite = new ObservableField<>(item.product.isFavorite());

        this.sizes = mItem.sizes;

    }

    public void onItemClick(){
        modelListener.onItemClick(this.mItem.product);
    }

    public void onItemFavorite(){
        modelListener.onItemFavorite(this.mItem.product);
    }

    public void onItemBasket(){
    }

    public interface ItemViewModelListener{
        void onItemClick(Product item);
        void onItemFavorite(Product item);
        void onItemBasket(Product item);
    }

}
