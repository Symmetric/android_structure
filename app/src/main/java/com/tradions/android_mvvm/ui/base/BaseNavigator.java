package com.tradions.android_mvvm.ui.base;

public interface BaseNavigator {

    void handleError(Throwable throwable);

}
