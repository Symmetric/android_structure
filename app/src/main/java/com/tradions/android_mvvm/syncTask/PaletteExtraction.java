package com.tradions.android_mvvm.syncTask;

import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.widget.ImageView;

import androidx.palette.graphics.Palette;

import java.lang.ref.WeakReference;

public class PaletteExtraction extends AsyncTask<Void, Void, Palette> {

    private WeakReference<ImageView> mImageViewWeakReference;
    private WeakReference<Bitmap> mBitmapWeakReference;

    /**
     * extract a palette from an image and apply the palette as
     * gradient to the background to the {@link ImageView}
     *
     * @param imageView the image view that the palette will be applied to
     * @param resource the {@link Bitmap} that will have the palette extracted from
     */
    public PaletteExtraction(ImageView imageView, Bitmap resource){
        this.mImageViewWeakReference = new WeakReference<>(imageView);
        this.mBitmapWeakReference = new WeakReference<>(resource);
    }

    @Override
    public Palette doInBackground(Void... voids) {

        if(mBitmapWeakReference.get() == null) return null;

        return Palette.from(mBitmapWeakReference.get()).generate();
    }

    @Override
    public void onPostExecute(Palette aPalette) {
        super.onPostExecute(aPalette);

        // get a reference to the context if it is still there
        ImageView imageView  = mImageViewWeakReference.get();
        if (imageView == null) return ;


        GradientDrawable gd = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                new int[] {aPalette.getMutedColor(0),aPalette.getLightMutedColor(0)});
        gd.setCornerRadius(0f);

        imageView.setBackground(gd);
    }


}

