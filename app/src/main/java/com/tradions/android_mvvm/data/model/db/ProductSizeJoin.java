package com.tradions.android_mvvm.data.model.db;

import com.google.gson.annotations.SerializedName;

public class ProductSizeJoin {

    @SerializedName("size_id")
    private long sizeId;

    @SerializedName("product_id")
    private long productId;

    @SerializedName("quantity")
    private int quantity;

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public long getProductId(){
        return this.productId;
    }

    public void setSizeId(long sizeId) {
        this.sizeId = sizeId;
    }

    public long getSizeId(){
        return this.sizeId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
