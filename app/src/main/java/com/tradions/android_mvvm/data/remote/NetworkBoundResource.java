package com.tradions.android_mvvm.data.remote;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.google.gson.stream.MalformedJsonException;
import com.tradions.android_mvvm.AndroidMVVMApp;
import com.tradions.android_mvvm.R;

import java.io.IOException;
import java.net.SocketTimeoutException;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;

public abstract class NetworkBoundResource<T, V> {

    private static final String TAG = "Network";
    private final MediatorLiveData<Resource<T>> result = new MediatorLiveData<>();
    private final MediatorLiveData<Resource<V>> resultServer = new MediatorLiveData<>();
    private Call<V> call = null;
    private LiveData<T> dbSource;


    @MainThread
    protected NetworkBoundResource() {
        setCall(createCall());
        setResultLoading();

        // Always load the data from DB intially so that we have

        if(shouldFetchLocal()) {
            dbSource = loadFromDb();
        } else{
            dbSource = new MutableLiveData<>();
        }

        // Fetch the data from network and add it to the resource
        if(!shouldFetchLocal()){
            fetchFromNetwork(dbSource);
        }
        else{
            result.addSource(dbSource, data -> {
                result.removeSource(dbSource);
                if (shouldFetch()) {
                    fetchFromNetwork(dbSource);
                } else if(shouldFetchLocal()){
                    result.addSource(dbSource, newData -> {
                        if(null != newData)
                            result.setValue(Resource.success(newData)) ;
                    });
                }
            });
        }

    }

    /**
     * This method fetches the data from remoted service and save it to local db
     * @param dbSource - Database source
     */
    private void fetchFromNetwork(final LiveData<T> dbSource) {
        Log.d(TAG, "fetchFromNetwork: I am here");
        result.addSource(dbSource, newData -> result.setValue(Resource.loading(newData)));

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                getCall().enqueue(new Callback<V>() {
                    @Override
                    public void onResponse(@NonNull Call<V> call, @NonNull Response<V> response) {
                        resultServer.setValue(Resource.success(response.body()));
                        if(shouldFetchLocal()) {
                            result.removeSource(dbSource);
                            saveResultAndReInit(response.body());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<V> call, @NonNull Throwable t) {
                        result.removeSource(dbSource);
                        resultServer.setValue(Resource.error(getCustomErrorMessage(t),null));
                        result.addSource(dbSource, newData -> result.setValue(Resource.error(getCustomErrorMessage(t), newData)));
                    }
                });
            }
        };

        (new Handler()).postDelayed(runnable, 1000);
    }

    private String getCustomErrorMessage(Throwable error){

        if (error instanceof SocketTimeoutException) {
            return AndroidMVVMApp.getAppContext().getString(R.string.requestTimeOutError);
        } else if (error instanceof MalformedJsonException) {
            return  AndroidMVVMApp.getAppContext().getString(R.string.responseMalformedJson);
        } else if (error instanceof IOException) {
             return  AndroidMVVMApp.getAppContext().getString(R.string.networkError);
        } else if (error instanceof HttpException) {
            return (((HttpException) error).response().message());
        } else {
            error.printStackTrace();
            return AndroidMVVMApp.getAppContext().getString(R.string.unknownError);
        }

    }

    @SuppressLint("StaticFieldLeak")
    @MainThread
    private void saveResultAndReInit(V response) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                saveCallResult(response);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                result.addSource(dbSource, newData -> {
                    if (null != newData) {
                        result.setValue(Resource.success(newData));
                    }

                });
            }
        }.execute();
    }

    @WorkerThread
    protected abstract void saveCallResult(V item);

    @MainThread
    public boolean shouldFetch() {
        return true;
    }

    // determine whether to fill resultServer variables from the server
    // and it will ignore the loadFromDb, and saveCallResult functions
    // you can get the result using the getAsLiveServerData() function.
    @MainThread
    public boolean shouldFetchLocal() {
        return true;
    }

    @NonNull
    @MainThread
    protected abstract LiveData<T> loadFromDb();

    @NonNull
    @MainThread
    protected abstract Call<V> createCall();

    public final LiveData<Resource<T>> getAsLiveData() {
        return result;
    }

    public final LiveData<Resource<V>> getAsLiveServerData() {
        return resultServer;
    }

    private void setResultLoading(){
        result.setValue(Resource.loading(null));
        resultServer.setValue(Resource.loading(null));
    }

    private void setResultError(String msg, T data){
        result.setValue(Resource.error(msg, data));
        resultServer.setValue(Resource.error(msg, null));
    }

    // a function to do a custom behaviour
    // for any custom variables
    public void customFunction(){

    }

    public Call<V> getCall() {
        return call;
    }

    public void setCall(Call<V> call) {
        this.call = call;
    }

    public void setDbSource(LiveData<T> dbSource) {
        result.removeSource(this.dbSource);
        this.dbSource = dbSource;
    }

    // refresh repository process, should update setCall and setSource
    // if parameter query parameters or server call parameters changed.
    public void refreshCall(){
        fetchFromNetwork(dbSource);
    }

}
