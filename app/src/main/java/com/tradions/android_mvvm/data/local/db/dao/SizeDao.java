package com.tradions.android_mvvm.data.local.db.dao;

import android.database.sqlite.SQLiteConstraintException;

import com.tradions.android_mvvm.data.model.db.Size;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public abstract class SizeDao implements BaseDao<Size> {
    @Query("SELECT * FROM sizes")
    public abstract LiveData<List<Size>> all();

    @Query("UPDATE sizes SET selected = 1 WHERE id = :id AND productId = :productId")
    public abstract void setSelected(long id, long productId);

    @Query("UPDATE sizes SET selected = 0")
    public abstract  void removeSelection();

    @Query("UPDATE sizes SET code = :code, quantity = :quantity, productId = :productId, selected = :selected" +
            " WHERE id = :id AND productId = :productId")

    abstract int update(long id, String code, int quantity, long productId, boolean selected);

    public void upsert(List<Size> items){
        try{
            save(items);
        }catch (SQLiteConstraintException exception){
            for (Size item: items ) {
                upsert(item, true);
            }
        }
    }

    public void upsert(Size item) {
        upsert(item, false);
    }
    /**
     * try to save item to database in case item already exist,
     * we update the item manually.
     *
     * @param item the item to be upsert
     * @param attemptedToSave if already tried to insert and field like the case upsert list
     */
    private void upsert(Size item, boolean attemptedToSave) {
        try {
            if (!attemptedToSave)
                save(item);
            else
                throw new SQLiteConstraintException();
        } catch (SQLiteConstraintException exception) {
            boolean updated = false;
            if (update(item.getId(),
                    item.getCode(),
                    item.getQuantity(),
                    item.getProductId(),
                    item.isSelected()) != 0) {
                updated = true;
            }
            if (!updated) {
                save(item);
            }
        }
    }
}
