package com.tradions.android_mvvm.data.local.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Update;

import java.util.List;

@Dao
public interface BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(List<T> items);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(T item);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(List<T> items);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(T item);

    @Delete
    void delete(T item);
}
