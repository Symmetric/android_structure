package com.tradions.android_mvvm.data.remote.repository;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

import com.tradions.android_mvvm.data.local.db.dao.ProductDao;
import com.tradions.android_mvvm.data.local.db.dao.SizeDao;
import com.tradions.android_mvvm.data.model.api.GenericListResponse;
import com.tradions.android_mvvm.data.model.api.GenericResponse;
import com.tradions.android_mvvm.data.model.db.Product;
import com.tradions.android_mvvm.data.model.db.ProductAllRelation;
import com.tradions.android_mvvm.data.remote.ApiService;
import com.tradions.android_mvvm.data.remote.NetworkBoundResource;
import com.tradions.android_mvvm.data.remote.Resource;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import retrofit2.Call;

public class ProductRepository {

    private final ProductDao itemDao;
    private final ApiService apiService;
    private NetworkBoundResource<List<ProductAllRelation>, GenericListResponse<Product>> loadAllNetworkBoundResource = null;

    boolean shouldFetch = false;

    private final String TAG = "product_repository";

    @Inject
    SizeDao sizeDao;

    @Inject
    ProductRepository(ProductDao dao, ApiService service) {
        this.itemDao = dao;
        this.apiService = service;
    }
    public LiveData<Resource<List<ProductAllRelation>>> loadAll(long brandId, int pageNumber) {
        Log.d("Products", "loadAll");
        if(loadAllNetworkBoundResource == null) {
            loadAllNetworkBoundResource = new NetworkBoundResource<List<ProductAllRelation>, GenericListResponse<Product>>() {
                @Override
                protected void saveCallResult(GenericListResponse<Product> item) {
                    if(null != item) {
                        itemDao.upsert(item.getData());

                        for (Product product : item.getData()) {
                            for (int i = 0 ; i < product.sizes.size() ; i++) {
                                product.sizes.get(i).setProductId(product.getId());
                                product.sizes.get(i).setQuantity(product.sizes.get(i).productSizeJoin.getQuantity());
                            }
                            sizeDao.upsert(product.sizes);
                        }
                    }
                }

                @NonNull
                @Override
                protected LiveData<List<ProductAllRelation>> loadFromDb() {
                    int limit = 12;
                    return itemDao.all(brandId, limit, limit * pageNumber);
                }

                @NonNull
                @Override
                protected Call<GenericListResponse<Product>> createCall() {
                    return apiService.productByBrand(brandId, pageNumber,0);
                }

            };
        }
        return loadAllNetworkBoundResource.getAsLiveData();
    }

    public void refreshLoadAll(long brandId, int pageNumber){
        if(loadAllNetworkBoundResource == null)
            return;

        int limit = 12;
        loadAllNetworkBoundResource.setDbSource(itemDao.all(brandId, limit, limit * pageNumber));
        loadAllNetworkBoundResource.setCall(apiService.productByBrand(brandId,pageNumber, 0));
        loadAllNetworkBoundResource.refreshCall();
    }

    public void favorite(long productId) {
        setFavorite(productId, true);
    }

    public void unFavorite(long productId) {
        setFavorite(productId, false);
    }

    @SuppressLint("StaticFieldLeak")
    public void setFavorite(long productId, boolean value){
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                itemDao.favorite(productId, value);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

            }
        }.execute();
    }

}
