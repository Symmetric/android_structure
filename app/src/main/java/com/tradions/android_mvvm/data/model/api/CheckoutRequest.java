package com.tradions.android_mvvm.data.model.api;


import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.google.gson.annotations.SerializedName;
import com.tradions.android_mvvm.BR;
import com.tradions.android_mvvm.data.model.db.Product;

import java.util.ArrayList;

public class CheckoutRequest extends BaseObservable {

    private float discount;
    private String notes;
    private float subtotal;
    private ArrayList<Product> products;

    @SerializedName("shipping__method_id")
    private long shippingMethodId;

    private float tax;
    @SerializedName("voucher_id")
    private long voucherId;

    @SerializedName("area_id")
    private long areaId;

    @SerializedName("total_price")
    private float totalPrice;

    @SerializedName("city_id")
    private long cityId;

    private String reasons;

    @SerializedName("delivery")
    private float deliveryFee;

    private String address;

    @SerializedName("requested__time_id")
    private long requestedTimeId;

    @SerializedName("requested_date")
    private String requestedDate;


    // Getter Methods

    @Bindable
    public float getDiscount() {
        return discount;
    }

    @Bindable
    public String getNotes() {
        return notes;
    }

    @Bindable
    public float getSubtotal() {
        return subtotal;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    @Bindable
    public float getTax() {
        return tax;
    }

    public String getReasons() {
        return reasons;
    }

    @Bindable
    public float getDeliveryFee() {
        return deliveryFee;
    }

    public String getAddress() {
        return address;
    }

    @Bindable
    public String getRequestedDate() {
        return requestedDate;
    }

    public long getAreaId() {
        return areaId;
    }

    public long getShippingMethodId() {
        return shippingMethodId;
    }

    public long getCityId() {
        return cityId;
    }

    @Bindable
    public float getTotalPrice() {
        return totalPrice;
    }

    public long getVoucherId() {
        return voucherId;
    }

    public long getRequestedTimeId() {
        return requestedTimeId;
    }

    // Setter Methods

    public void setDiscount(float discount) {
        this.discount = discount;
        this.notifyPropertyChanged(BR.totalPrice);
    }

    public void setNotes(String notes) {
        this.notes = notes;
        this.notifyPropertyChanged(BR.notes);
    }

    public void setSubtotal(float subtotal) {
        this.subtotal = subtotal;
        this.notifyPropertyChanged(BR.subtotal);
    }

    public void setTax(float tax) {
        this.tax = tax;
        this.notifyPropertyChanged(BR.tax);
    }
    public void setReasons(String reasons) {
        this.reasons = reasons;
    }

    public void setDeliveryFee(float deliveryFee) {
        this.deliveryFee = deliveryFee;
        this.notifyPropertyChanged(BR.deliveryFee);
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setRequestedDate(String requestedDate) {
        this.requestedDate = requestedDate;
        this.notifyPropertyChanged(BR.requestedDate);
    }

    public void setAreaId(long areaId) {
        this.areaId = areaId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public void setRequestedTimeId(long requestedTimeId) {
        this.requestedTimeId = requestedTimeId;
    }

    public void setShippingMethodId(long shippingMethodId) {
        this.shippingMethodId = shippingMethodId;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
        this.notifyPropertyChanged(BR.totalPrice);
    }

    public void setVoucherId(long voucherId) {
        this.voucherId = voucherId;
    }
}

