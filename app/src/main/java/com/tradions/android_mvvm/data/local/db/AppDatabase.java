/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.tradions.android_mvvm.data.local.db;

import com.tradions.android_mvvm.data.local.db.dao.ProductDao;
import com.tradions.android_mvvm.data.local.db.dao.SizeDao;
import com.tradions.android_mvvm.data.model.db.Product;
import com.tradions.android_mvvm.data.model.db.Size;

import androidx.room.Database;
import androidx.room.RoomDatabase;


@Database(entities = {
        Size.class,
        Product.class,
        }, version = 27)
public abstract class AppDatabase extends RoomDatabase {

    public abstract ProductDao productDao();

    public abstract SizeDao sizeDao();
}
