package com.tradions.android_mvvm.data.model.api;

public interface GenericResponseInterface<T> {

    public T getData();
}
