package com.tradions.android_mvvm.data.remote;


import com.tradions.android_mvvm.data.model.api.GenericListResponse;
import com.tradions.android_mvvm.data.model.api.GenericResponse;
import com.tradions.android_mvvm.data.model.db.Product;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {
    @GET("product")
    Call<GenericListResponse<Product>> products();

    @GET("products-by-brand/{brand_id}/{limit}/{gender_id}")
    Call<GenericListResponse<Product>> productByBrand(@Path("brand_id") long brandId, @Path("limit") int pageNumber, @Path("gender_id") long genderId);

    @GET("get-product/{product_id}")
    Call<GenericResponse<Product>> product(@Path("product_id") long productId);
}

