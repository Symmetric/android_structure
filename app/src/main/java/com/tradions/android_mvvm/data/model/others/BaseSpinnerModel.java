package com.tradions.android_mvvm.data.model.others;

public interface BaseSpinnerModel {

    String getName();

    long getObjectId();

}
