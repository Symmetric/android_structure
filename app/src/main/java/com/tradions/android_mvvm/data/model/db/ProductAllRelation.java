package com.tradions.android_mvvm.data.model.db;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import androidx.room.Embedded;
import androidx.room.Relation;

public class ProductAllRelation {
    @Embedded
    public Product product;

    @Relation(parentColumn = "id", entityColumn = "productId", entity = Size.class)
    @SerializedName("size")
    public List<Size> sizes;
}
