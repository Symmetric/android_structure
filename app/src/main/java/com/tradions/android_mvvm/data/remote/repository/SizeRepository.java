package com.tradions.android_mvvm.data.remote.repository;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import com.tradions.android_mvvm.data.local.db.dao.SizeDao;
import com.tradions.android_mvvm.data.model.db.Size;
import com.tradions.android_mvvm.data.remote.ApiService;

import javax.inject.Inject;

public class SizeRepository {

    private final SizeDao itemDao;
    private final ApiService apiService;

    @Inject
    SizeRepository( SizeDao dao, ApiService service) {
        this.itemDao = dao;
        this.apiService = service;
    }

    @SuppressLint("StaticFieldLeak")
    public void changeSelection(Size item) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                itemDao.removeSelection();
                itemDao.setSelected(item.getId(), item.getProductId());
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

            }
        }.execute();
    }
}
