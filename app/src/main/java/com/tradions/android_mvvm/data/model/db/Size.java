package com.tradions.android_mvvm.data.model.db;

import com.google.gson.annotations.SerializedName;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;

@Entity(tableName = "sizes",
        primaryKeys = { "id", "productId" } )
public class Size {

    @SerializedName("id")
    private long id;

    @SerializedName("code")
    private String code;

    @ColumnInfo(name = "quantity")
    private int quantity;

    @ColumnInfo(name = "productId")
    private transient long productId;

    @ColumnInfo(name = "selected")
    private transient boolean selected = false;

    @Ignore
    @SerializedName("pivot")
    public ProductSizeJoin productSizeJoin;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
