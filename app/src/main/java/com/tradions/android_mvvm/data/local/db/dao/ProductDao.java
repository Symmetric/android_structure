package com.tradions.android_mvvm.data.local.db.dao;

import android.database.sqlite.SQLiteConstraintException;

import com.tradions.android_mvvm.data.model.db.Product;
import com.tradions.android_mvvm.data.model.db.ProductAllRelation;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

@Dao
public abstract class ProductDao implements BaseDao<Product> {
    @Query("SELECT products.* FROM products")
    @Transaction
    public abstract LiveData<List<ProductAllRelation>> all();

    @Query("SELECT products.* FROM products WHERE brandId = :brandId LIMIT :limit OFFSET :offset")
    @Transaction
    public abstract LiveData<List<ProductAllRelation>> all(long brandId, int limit, int offset);

    @Query("SELECT products.* FROM products WHERE id = :productId")
    @Transaction
    public abstract LiveData<ProductAllRelation> get(long productId);

    @Insert(onConflict = OnConflictStrategy.FAIL)
    public abstract void save(List<Product> products);

    @Insert(onConflict = OnConflictStrategy.FAIL)
    public abstract void save(Product products);

    @Query("UPDATE products SET favorite = :value" +
            " WHERE id = :id")
    public abstract int favorite(long id, boolean value);

//    @Query("INSERT INTO cart_products (productId) VALUES (:productId)")
//    public abstract int addProduct(long productId);

    @Query("UPDATE OR REPLACE products SET name = :name,description = :description,price = :price,image = :image,thumbnail = :thumbnail,discount = :discount,discountPercent = :discountPercent,active = :active,love = :love,genderId = :genderId,brandId = :brandId,categoryId = :categoryId,offerId = :offerId" +
            " WHERE id = :id")
    abstract int update(long id, String name, String description, String price, String image, String thumbnail, long discount, long discountPercent, boolean active, boolean love, long genderId, long brandId, long categoryId, long offerId);


    public void upsert(List<Product> products) {
        try {
            save(products);
        } catch (SQLiteConstraintException exception) {
            for (Product product : products) {
                upsert(product, true);
            }
        }
    }

    public void upsert(Product product) {
        upsert(product, false);
    }

    /**
     * try to save it to database in case item already item,
     * we update the item manually.
     *
     * @param product the product to be upsert
     * @param attemptedToSave if already tried to insert and field like the case upsert list
     */
    private void upsert(Product product, boolean attemptedToSave) {
        try{
            if(!attemptedToSave)
                save(product);
            else
                throw new SQLiteConstraintException();
        }catch (SQLiteConstraintException exception){
            boolean updated = false;
            if(update(product.getId(),
                    product.getName(),
                    product.getDescription(),
                    product.getPrice(),
                    product.getImage(),
                    product.getThumbnail(),
                    product.getDiscount(),
                    product.getDiscountPercent(),
                    product.isActive(),
                    product.isLove(),
                    product.getGenderId(),
                    product.getBrandId(),
                    product.getCategoryId(),
                    product.getOfferId()) != 0)
            {
                updated = true;
            }
            if(!updated){
                save(product);
            }
        }
    }

}
