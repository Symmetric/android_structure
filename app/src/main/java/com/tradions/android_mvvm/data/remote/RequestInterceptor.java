package com.tradions.android_mvvm.data.remote;


import java.io.IOException;

import androidx.annotation.NonNull;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RequestInterceptor implements Interceptor {

    @Override
    public Response intercept(@NonNull Interceptor.Chain chain) throws IOException {
        Request originalRequest = chain.request();
        HttpUrl originalHttpUrl = originalRequest.url();

        HttpUrl url = originalHttpUrl.newBuilder()
                .build();

        Request request = originalRequest
                .newBuilder()
                .addHeader("Authorization", ApiConstants.API_KEY)
                .addHeader("lang", "en")
                .addHeader("Accept", "application/json")
                .url(url)
                .build();

        return chain.proceed(request);
    }
}