package com.tradions.android_mvvm.data.remote;


public class ApiConstants {
    public static final String BASE_URL = "http://example.com";
    public static final String API_BASE_URL = BASE_URL + "/api/";
    public static final long CONNECT_TIMEOUT = 30000;
    public static final long READ_TIMEOUT = 30000;
    public static final long WRITE_TIMEOUT = 30000;
    public static final String API_KEY = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjRhODA1MDQ1NzY5OTcyNjM3MjI2Yjg0MDEzZmQxMWI0NTI0YzY1MDg3YWU2MDdmNDRiNTU4MzdlMjUzMTEyMjRmMjhiNjEyNTZiYzFiZGI0In0.eyJhdWQiOiIzIiwianRpIjoiNGE4MDUwNDU3Njk5NzI2MzcyMjZiODQwMTNmZDExYjQ1MjRjNjUwODdhZTYwN2Y0NGI1NTgzN2UyNTMxMTIyNGYyOGI2MTI1NmJjMWJkYjQiLCJpYXQiOjE1NjA0MjY4NjIsIm5iZiI6MTU2MDQyNjg2MiwiZXhwIjoxNTkyMDQ5MjYyLCJzdWIiOiIxMCIsInNjb3BlcyI6W119.Cskygn8olT90PPBOJacPEPzRp27apW4dB4KZHqM8rsOAHnlSIDRk5IUrLE9pd6ct7ucUR-EFHU40bKUpOOQV4H63UE7zHEp2M-Yp3v9NzaLmAFbq8J5mZl0zxG97OD2WZDi8QEmDyi17L_doI82y1sgYT7wt_fP_TpPi1jh8BKSq7slUlktSQw5j0h2FLHjCUvaoPm3kGILbTtx4FhwJwJz_DEFNGq2fvkW57TYspIprfAMri7_iPuEKWfPtRnCqBowYnwUQwQUPklhI5ctIDI4nUih8eJWoBbxp78clOY4ScLryLg5etMsY56MiSHQ4Dno7VG-Zn3SA4nU1Qnp791i0BgpDu26HYg1l-tYDYSa25xoywndMTurh-A68DnnzSFj10XStS7sBwwYxDAOmVFDrpz-Bz6GOFj86fsJKWANeeQl6pVMx1szHzNCaN5sEIgcXlfxeL9JJ3TUUNP4_CM91bsh4dN7tUu_YW1fr4Uh-mD-qASD0HqaG_4SJIkQLx0m4VmdNX_Yv0V3agBFZTA6hnr9yKp_nJlDCbcJpn69dTTW0yvpkZiQnLx0GKoQf1UzgmyI8Qie-UqGlLPCCpiimVLlvGKQbEn7ksKMLI_htoPsQZmJ9yUlxGIsDlA0oneOBgXtnDARNDrz6XtKv7Q6sAsE12-wPDCZBDYJpLZU";

    private ApiConstants(){
        // Private constructor to hide the implicit one
    }

}
